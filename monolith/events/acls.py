from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    params = {"query": f"{city}, {state}", "per_page": 1}
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers, params=params)
    content = json.loads(response.content)
    return content["photos"][0]["url"]


def get_weather(city, state):
    params = {"q": f"{city},{state},US", "appid": OPEN_WEATHER_API_KEY}
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    return {
        "description": content["weather"][0]["description"],
        # "temp": current_weather["main"]["temp"],
    }


# def get_weather_data(city, state):
#     weather_object = requests.get(
#         "https://api.openweathermap.org/data/2.5/weather?q={city},{state},{'USA'}&appid={OPEN_WEATHER_API_KEY}",
#         city,
#         state,
#         OPEN_WEATHER_API_KEY,
#     )
#     weather = weather_object["weather"]["description"]
#     return weather
