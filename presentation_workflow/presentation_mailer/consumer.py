import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:
        def process_approval_message(ch, method, properties, body):
            body_data = json.loads(body)
            name = body_data.get("presenter_name")
            recipients = [body_data.get("presenter_email")]
            title = body_data.get("title")
            subject = "Your presentation has been accepted"
            message_body = f"{name}, we're happy to tell you that your presentation {title} has been accepted"
            send_mail(
                subject=subject,
                message=message_body,
                from_email="admin@conference.go",
                recipient_list=recipients,
                fail_silently=False,
            )


        def process_rejection_message(ch, method, properties, body):
            body_data = json.loads(body)
            name = body_data.get("presenter_name")
            recipients = [body_data.get("presenter_email")]
            title = body_data.get("title")
            subject = "Your presentation has been rejected"
            message_body = f"{name}, we're saddened to tell you that your presentation {title} has been rejected"
            send_mail(
                subject=subject,
                message=message_body,
                from_email="admin@conference.go",
                recipient_list=recipients,
                fail_silently=False,
            )

        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="presentation_rejections")
        channel.queue_declare(queue="presentation_approvals")
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval_message,
            auto_ack=True,
        )
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejection_message,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)